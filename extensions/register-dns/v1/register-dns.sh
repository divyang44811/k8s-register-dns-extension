#!/bin/bash
# Add support for registering host on dns server with keys

set -e

apt-get -y install jq

KEY=/root/Kdns.key
parameters=$(echo $1 | base64 -d -)

get_param() {
  local param=$1
  echo $(echo "$parameters" | jq ".$param" -r)
}

domainname=$(get_param "DOMAINNAME")
dns_key=$(get_param "DNS_KEY")
dns_private_key=$(get_param "DNS_PRIVATE_KEY")
# optional
ssh_config=$(get_param "SSH_CONFIG")

echo "$dns_key" > /root/Kdns.key
echo -e "$dns_private_key" > /root/Kdns.private


echo $(date) " - Starting Script"

cat > /etc/network/if-up.d/register-dns <<EOFDHCP
#!/bin/sh

# only execute on the primary nic
if [ "\${IFACE}" = "eth0" ]
then
    ip=\$(ip address show eth0 | awk '/inet / {print \$2}' | cut -d/ -f1)
    host=\$(hostname -s)
    nsupdatecmds=/var/tmp/nsupdatecmds
    echo "update delete \${host}.${domainname} a" > \$nsupdatecmds
    echo "update add \${host}.${domainname} 3600 a \${ip}" >> \$nsupdatecmds
    echo "send" >> \$nsupdatecmds

    nsupdate  -k $KEY \$nsupdatecmds
fi
EOFDHCP

chmod 755 /etc/network/if-up.d/register-dns

if ! grep -Fq "${domainname}" /etc/dhcp/dhclient.conf
then
    echo $(date) " - Adding domain to dhclient.conf"

    echo "supersede domain-name \"${domainname}\";" >> /etc/dhcp/dhclient.conf
    echo "prepend domain-search \"${domainname}\";" >> /etc/dhcp/dhclient.conf
fi

# service networking restart
echo $(date) " - Restarting network"
sudo ifdown eth0 && sudo ifup eth0

#
# Specific optional task, only work with our git server
#

if [ "$ssh_config" != "" ]
then
  wget http://ca.services.local/Infra_CA.crt -O /usr/local/share/ca-certificates/Infra_CA.crt
  /usr/sbin/update-ca-certificates

  wget https://gitrep.services.local/cloud/acs-engine-k8s-postconf/raw/master/acs-engine-k8s-postconf.sh -O /root/acs-engine-k8s-postconf.sh
  chmod +x /root/acs-engine-k8s-postconf.sh
  /root/acs-engine-k8s-postconf.sh
fi
